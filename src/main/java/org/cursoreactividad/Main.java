package org.cursoreactividad;

import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println(maxAndMin(List.of(2,6,7,1), "min"));
        System.out.println(discount(12000.00, 0.1));
        iva();

    }

    /**
     * 1. Con la intención de encontrar el número mayor en una lista y a su vez el número menor, cree un método que le permita
     * estas dos funciones, siguiendo los principios de las funciones puras.
     * */
    private static Integer maxAndMin(List<Integer> numbers, String op) {
       return op.equals("max") ? Collections.max(numbers) : Collections.min(numbers);
    }

    /**
     * 2. En una tienda de ropa necesitamos aplicar un descuento a las ventas que se hacen, este descuento
     * es variable por lo que necesitamos que usted cree un método que reciba el total de la venta y el porcentaje a descontar
     * y devuelva el nuevo precio total.
     * */
    private static Double discount(Double saleValue, Double discountValue) {
        return saleValue - saleValue * discountValue;
    }

    /**
     * 3. Necesitamos calcular el iva de una venta, para esto es necesario que usted cree un método
     * Tendremos dos escenarios posibles, en el primero se le pasará únicamente el valor al cual se le debe
     * aplicar el iva, sin especificar el porcentaje, en este caso se debe aplicar un 21% siempre.
     * En el segundo escenario se le pasará tanto el precio de la venta como el porcentaje de iva.
     * Cree una sola función que pueda lograr estos dos propósitos
     * */
    private static void iva() {
        Iva iva = new IvaImpl();
        System.out.println(iva.setIva(12000.0));
        System.out.println(iva.setIva(12000.0, 0.19));
    }
}