package org.cursoreactividad;

public interface Iva {

    Double setIva(Double value);

    Double setIva(Double value, Double iva);
}
