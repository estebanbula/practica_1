package org.cursoreactividad;

public class IvaImpl implements Iva{

    @Override
    public Double setIva(Double value) {
        return value * 0.21;
    }

    @Override
    public Double setIva(Double value, Double iva) {
        return value * iva;
    }
}
